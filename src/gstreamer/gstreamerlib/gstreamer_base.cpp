#pragma once

#include "gstreamer_base.h"

using namespace adtf::base;
using namespace adtf::ucom;
using namespace adtf::filter;

static gboolean bus_call(GstBus *bus, GstMessage *msg, gpointer data)
{
    switch (GST_MESSAGE_TYPE(msg))
    {

    case GST_MESSAGE_EOS:
        LOG_INFO("End of stream\n");
        break;

    case GST_MESSAGE_ERROR:
    {
        gchar  *debug;
        GError *error;

        gst_message_parse_error(msg, &error, &debug);
        g_free(debug);

        LOG_ERROR("%s", error->message);
        g_error_free(error);

        break;
    }

    case GST_MESSAGE_WARNING:
    {
        gchar  *debug;
        GError *warning;

        gst_message_parse_warning(msg, &warning, &debug);
        g_free(debug);

        LOG_WARNING("%s", warning->message);
        g_error_free(warning);

        break;
    }

    case GST_MESSAGE_INFO:
    {
        gchar  *debug;
        GError *info;

        gst_message_parse_info(msg, &info, &debug);
        g_free(debug);

        LOG_INFO("%s", info->message);
        g_error_free(info);

        break;
    }

    default:
        break;
    }

    return TRUE;
}

static void print_cap_from_pad(GstPad* pad)
{
    GstCaps* pCaps = gst_pad_get_current_caps(pad);
    GstStructure* pCapsStruct = gst_caps_get_structure(pCaps, 0);

    if (!pCapsStruct)
    {
        LOG_ERROR("gst_caps had NULL structure");
        return ;
    }

    LOG_INFO("Struct %s", gst_structure_get_name(pCapsStruct));   
}

static cString get_property_string(GstPad* pad, const gchar * fieldname)
{
    GstCaps* pCaps = gst_pad_get_current_caps(pad);
    GstStructure* pCapsStruct = gst_caps_get_structure(pCaps, 0);
    return gst_structure_get_string(pCapsStruct, "media");
}

static void on_pad_added(GstElement* element, GstPad* pad, gpointer data) {
    GstPad* sinkpad;
    GstElement* targetElement = (GstElement*)data;

    LOG_INFO("Pad added '%s' to element '%s' => '%s'", gst_pad_get_name(pad), gst_element_get_name(element), gst_element_get_name(targetElement));
    
    sinkpad = gst_element_get_static_pad(targetElement, "sink");
    if (!sinkpad)
    {
        LOG_ERROR("  => Sink pad 'sink' not found");
        return;
    }

    if (gst_pad_is_linked(sinkpad)) 
    {
        LOG_ERROR("  => We are already linked. Ignoring.\n");
        return;
    }

    auto mediaType = get_property_string(pad, "media");
    if (mediaType != "video" && mediaType != "")
    {
        LOG_ERROR("  => Reject none video type '%s'", mediaType.GetPtr());
        return;
    }

    if (!gst_element_link_pads(element, gst_pad_get_name(pad), targetElement, "sink"))
    {
        LOG_ERROR("   => Failed");
        print_cap_from_pad(pad);
        print_cap_from_pad(sinkpad);

        GstCaps* pCapsSource = gst_pad_get_current_caps(pad);
        GstCaps* pCapsSink = gst_pad_get_allowed_caps(sinkpad);

        if (!gst_caps_is_always_compatible(pCapsSource, pCapsSink))
        {
            LOG_ERROR("Not compatible");

            LOG_ERROR(gst_caps_to_string(pCapsSource));
            LOG_ERROR(gst_caps_to_string(pCapsSink));
        }
        else
        {
            LOG_ERROR("Compatible");
        }
    }
    else
    {
        LOG_INFO("   => Success");
    }
    gst_object_unref(sinkpad);
}



cGStreamerBaseFilter::cGStreamerBaseFilter(tBool bRegisterProperty)
{
    m_pGStreamerPipeClient = CreateInterfaceClient<IGStreamerPipe>("in");
    m_pGStreamerPipeServer = make_object_ptr<cGStreamerPipe>(this);
    CreateInterfaceServer("out", m_pGStreamerPipeServer);

    if (bRegisterProperty)
    {
        m_strElementFactory.SetDescription("Name of the GStreamer Factory");
        RegisterPropertyVariable("element_factory", m_strElementFactory);

        RegisterPropertyVariable("last_pipeline_element", m_bLastPipeElement);
        RegisterPropertyVariable("dynamic_pad", m_bDynamicPad);
    }

    SetDescription("Use this filter to create one instance of a GStreamer Element.");
}

cGStreamerBaseFilter::~cGStreamerBaseFilter()
{
    if (m_pPipeline)
    {
        gst_element_set_state(m_pPipeline, GST_STATE_NULL);
        gst_object_unref(GST_OBJECT(m_pPipeline));
    }
}

tResult cGStreamerBaseFilter::Init(tInitStage eStage)
{
    RETURN_IF_FAILED(cFilter::Init(eStage));

    switch (eStage)
    {
    case tInitStage::StageFirst:
    {

    }
    break;
    case tInitStage::StagePreConnect:
    {
        
        CreateElement();
        InitProperties();
    }
    break;
    case tInitStage::StagePostConnect:
    {
        if (m_bLastPipeElement)
        {
            
            LOG_INFO("--- Create GStreamer Pipeline ---");

            m_pPipeline = gst_pipeline_new("adtf_pipeline");

            RETURN_IF_POINTER_NULL_DESC(m_pPipeline, "Error while creating pipeline");

            auto pBus = gst_pipeline_get_bus(GST_PIPELINE(m_pPipeline));
            gst_bus_add_watch(pBus, bus_call, this);
            gst_object_unref(pBus);

            if (!gst_bin_add(GST_BIN(m_pPipeline), m_pElement))
            {
                RETURN_ERROR_DESC(ERR_NOT_CONNECTED, "gst_bin_add failed");
            }

            if (m_pGStreamerPipeClient.IsValid())
            {
                RETURN_IF_FAILED(m_pGStreamerPipeClient->Connect(this, this));
            }

            RETURN_IF_FAILED(InitElement(m_pElement));
            /*if (GST_STATE_CHANGE_SUCCESS != gst_element_set_state(m_pPipeline, GST_STATE_PLAYING))
            {
                LOG_ERROR("Failed to go to ready");
                RETURN_ERROR_DESC(ERR_INVALID_STATE, "Failed to go to ready");
            }*/

            gst_element_set_state(m_pPipeline, GST_STATE_READY);
            
        }
    }
    break;
    }

    RETURN_NOERROR;
}

tResult cGStreamerBaseFilter::Start()
{
    
    if (m_bLastPipeElement)
    {
        LOG_INFO("--- Start Playing Gstreamer Pipeline ---");
        gst_element_set_state(m_pPipeline, GST_STATE_PLAYING);
    }
    return cFilter::Start();
}

void cGStreamerBaseFilter::CreateElement()
{
    m_pElement = gst_element_factory_make((*m_strElementFactory).GetPtr(), (*m_strName).GetPtr());

    if (!m_pElement)
    {
        THROW_ERROR_DESC(ERR_FAILED, "Could not create GStreamer Element %s ", (*m_strName).GetPtr());
    }
}

void cGStreamerBaseFilter::Link(cGStreamerBaseFilter * pDestFilter)
{
    if (!gst_element_link(m_pElement, pDestFilter->m_pElement))
    {
        //RETURN_ERROR_DESC(ERR_NOT_CONNECTED, "gst_element_link failed %s %s", m_strName->GetPtr(), pParentFilter->m_strName->GetPtr());
        LOG_ERROR("gst_element_link failed %s %s", m_strName->GetPtr(), pDestFilter->m_strName->GetPtr());
    }
    else
    {
        LOG_DUMP("gst_element_link success %s %s", m_strName->GetPtr(), pDestFilter->m_strName->GetPtr());
    }
}

void cGStreamerBaseFilter::InitProperties()
{
    m_oGStreamerReflection.ParseElement(m_pElement);

    for (auto & oProperty : m_oGStreamerReflection.GetProperties())
    {
        cString strName = ("gst_" + oProperty.first).c_str();
        cString strValue = get_property<cString>(*this, strName.GetPtr(), "");

        if (oProperty.first == "name") continue;

        if (strValue == "")
        {
            set_property<cString>(*this, strName.GetPtr(), oProperty.second.value.c_str());
        }
        else
        {
            if (oProperty.first == "caps")
            {
                LOG_INFO("set properties %s from %s = %s", oProperty.first.c_str(), m_strName->GetPtr(), strValue.GetPtr());
                GstCaps *pCaps = gst_caps_from_string(strValue.GetPtr());
                if (!pCaps)
                {
                    THROW_ERROR_DESC(ERR_FAILED, "Could not create caps %s", strValue.GetPtr());
                }
                LOG_INFO(gst_caps_to_string(pCaps));
                g_object_set(m_pElement, "caps", pCaps, NULL);
                gst_caps_unref(pCaps);
            }
            else
            {
                if (strValue == "true")
                {
                    g_object_set(m_pElement, oProperty.first.c_str(), true, NULL);
                }
                else if (strValue == "false")
                {
                    g_object_set(m_pElement, oProperty.first.c_str(), false, NULL);
                }
                else
                {
                    LOG_INFO("set properties %s from %s = %s", oProperty.first.c_str(), m_strName->GetPtr(), strValue.GetPtr());
                
                    g_object_set(m_pElement, oProperty.first.c_str(), strValue.GetPtr(), NULL);
                }
            }
        }
    }
}


tResult cGStreamerBaseFilter::InitElement(GstElement* pElement)
{
    RETURN_NOERROR;
}

tResult cGStreamerBaseFilter::AddGStreamerFilter(cGStreamerBaseFilter * pDestFilter, cGStreamerBaseFilter * pRootFilter)
{
    RETURN_IF_FAILED(InitElement(m_pElement));

    if (!gst_bin_add(GST_BIN(pRootFilter->m_pPipeline), m_pElement))
    {
        RETURN_ERROR_DESC(ERR_NOT_CONNECTED, "gst_bin_add failed");
    }

    if (m_pGStreamerPipeClient.IsValid())
    {
        RETURN_IF_FAILED(m_pGStreamerPipeClient->Connect(this, pRootFilter));
    }

    if (pDestFilter->m_pElement && m_pElement)
    {
        if (m_bDynamicPad)
        {
            LOG_DUMP("Signal pad-added to %s", this->m_strName->GetPtr());
            g_signal_connect(m_pElement, "pad-added", G_CALLBACK(on_pad_added), pDestFilter->m_pElement);
        }
        else
        {
            Link(pDestFilter);
        }
    }

    RETURN_NOERROR;
}