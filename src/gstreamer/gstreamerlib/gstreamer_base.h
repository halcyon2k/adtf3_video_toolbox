#pragma once

#include "gstreamer_reflection.h"
#include <adtffiltersdk/adtf_filtersdk.h>

static gboolean bus_call(GstBus *bus, GstMessage *msg, gpointer data);
static void print_cap_from_pad(GstPad* pad);
static cString get_property_string(GstPad* pad, const gchar * fieldname);
static void on_pad_added(GstElement* element, GstPad* pad, gpointer data);

class cGStreamerBaseFilter;

class IGStreamerPipe : public adtf::ucom::IObject
{
public:
    ADTF_IID(IGStreamerPipe, "gstreamer_pipe.gstreamer.adtf.iid");

public:
    virtual tResult Connect(cGStreamerBaseFilter * pParentFilter, cGStreamerBaseFilter * pRootFilter) = 0;
};

class cGStreamerBaseFilter : public adtf::filter::cFilter
{
public:
    adtf::base::property_variable<cString> m_strElementFactory = { "$(THIS_OBJECT_NAME)" };
    adtf::base::property_variable<cString> m_strName = { "my_$(THIS_OBJECT_NAME)" };
    adtf::base::property_variable<tBool> m_bLastPipeElement = tFalse;
    adtf::base::property_variable<tBool> m_bDynamicPad = tFalse;

    GstElement* m_pElement = nullptr;
    GstElement* m_pPipeline = nullptr;

    cGStreamerReflection m_oGStreamerReflection;

    adtf::ucom::object_ptr<IGStreamerPipe> m_pGStreamerPipeServer;
    adtf::filter::interface_client<IGStreamerPipe> m_pGStreamerPipeClient;


public:

    class cGStreamerPipe : public adtf::ucom::object<IGStreamerPipe>
    {

    public:
        cGStreamerBaseFilter* m_pGStreamerFilter;


    public:
        cGStreamerPipe(cGStreamerBaseFilter* pGStreamerFilter) : m_pGStreamerFilter(pGStreamerFilter)
        {

        }

        tResult Connect(cGStreamerBaseFilter * pParentFilter, cGStreamerBaseFilter * pRootFilter)
        {
            return m_pGStreamerFilter->AddGStreamerFilter(pParentFilter, pRootFilter);
        }
    };

public:

    cGStreamerBaseFilter(tBool bRegisterProperty = tTrue);
    ~cGStreamerBaseFilter();

    tResult Init(tInitStage eStage);
    tResult Start() override;
    virtual void CreateElement();
    virtual void Link(cGStreamerBaseFilter * pDestFilter);
    virtual void InitProperties();


    virtual tResult InitElement(GstElement* pElement);
    virtual tResult AddGStreamerFilter(cGStreamerBaseFilter * pDestFilter, cGStreamerBaseFilter * pRootFilter);

private:
    adtf::filter::interface_client<IGStreamerPipe> m_oInterfaceClient;
};