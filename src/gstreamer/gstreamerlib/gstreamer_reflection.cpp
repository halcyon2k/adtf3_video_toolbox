/**
 * Copyright 2019 Sebastian Geißler <mail@sebastiangeissler.de>
 *
 * (https://opensource.org/licenses/MIT)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


#include "gstreamer_reflection.h"

using namespace adtf::util;

std::string gchar_to_string(const gchar * strInput)
{
    if (strInput)
    {
        return std::string(strInput);
    }
    return std::string();
}

std::map<std::string, cGStreamerReflection::cGStreamerProperty>& cGStreamerReflection::GetProperties()
{
    return m_mapProperties;
}

void cGStreamerReflection::ParseElement(std::string strElement)
{
    auto pPluginFeature = gst_registry_lookup_feature(gst_registry_get(), strElement.c_str());

    if (GST_IS_ELEMENT_FACTORY(pPluginFeature)) 
    {
        GstElementFactory *pFactory;

        pFactory = GST_ELEMENT_FACTORY(pPluginFeature);
        GstElement* pElement = gst_element_factory_create(pFactory, "TempObject");
        ParseElement(pElement);
    }
    else
    {
        THROW_ERROR_DESC(ERR_NOT_FOUND, "Couldn't find feature for element' '%s'", strElement.c_str());
    }
}

void cGStreamerReflection::ParseElement(GstElement * element)
{
    g_return_if_fail(element != NULL);

    m_strAuthor = GetMetadata(element, GST_ELEMENT_METADATA_AUTHOR);
    m_strDescription = GetMetadata(element, GST_ELEMENT_METADATA_DESCRIPTION);
    m_strDocu = GetMetadata(element, GST_ELEMENT_METADATA_DOC_URI);
    m_strIconName = GetMetadata(element, GST_ELEMENT_METADATA_ICON_NAME);
    m_strKlass = GetMetadata(element, GST_ELEMENT_METADATA_KLASS);
    m_strLongName = GetMetadata(element, GST_ELEMENT_METADATA_LONGNAME);

    ParseElementProperties(element);
    ParseElementPads(element);
}

void cGStreamerReflection::ParseElementProperties(GstElement * element)
{
    
    GParamSpec  **propertySpecs;

    m_mapProperties.clear();
    guint properties;
    propertySpecs = g_object_class_list_properties(
        G_OBJECT_GET_CLASS(element),
        &properties);

    for (guint i = 0; i < properties; i++)
    {
        GValue      value = { 0, };
        GParamSpec  *param = propertySpecs[i];

        bool readable = false;
        bool writable = false;
        bool controlable = false;

        g_value_init(&value, param->value_type);

        if (param->flags & G_PARAM_READABLE)
        {
            g_object_get_property(G_OBJECT(element), param->name, &value);
            readable = true;
        }

        if (param->flags & G_PARAM_WRITABLE)
        {
            writable = true;
        }
        if (param->flags & GST_PARAM_CONTROLLABLE)
        {
            controlable = true;
        }

        std::string name = g_param_spec_get_name(param);

        std::string propertyValue;
        std::string propertyDefaultValue;
        std::string propertyType;

        tFloat64 minimum = 0.0;
        tFloat64 maximum = 0.0;

        switch (G_VALUE_TYPE(&value))
        {
        case G_TYPE_STRING:
        {
            GParamSpecString *pString = G_PARAM_SPEC_STRING(param);
            
            if (readable)
            {
                if(auto pPropertyValue = g_value_get_string(&value))
                {
                    propertyValue = pPropertyValue;
                }
            }
            if(pString->default_value)
            {
                propertyDefaultValue = pString->default_value;
            }
            
            propertyType = "cString";
            break;
        }

        case G_TYPE_BOOLEAN: //  Boolean
        {
            GParamSpecBoolean *pBoolean = G_PARAM_SPEC_BOOLEAN(param);
            if (readable)
                propertyValue = g_value_get_boolean(&value) ? "true" : "false";
            propertyDefaultValue = pBoolean->default_value ? "true" : "false";
            propertyType = "tBool";
            break;
        }

        case G_TYPE_ULONG:  //  Unsigned Long
        {
            GParamSpecULong *pULong = G_PARAM_SPEC_ULONG(param);
            if (readable)                                        /* current */
                propertyValue = cString::FromType((tUInt64)g_value_get_ulong(&value));

            minimum = pULong->minimum;
            maximum = pULong->maximum;
            propertyDefaultValue = cString::FromType((tUInt64)pULong->default_value);
            propertyType = "tUInt64";
            break;
        }

        case G_TYPE_LONG:  //  Long
        {
            GParamSpecLong *pLong = G_PARAM_SPEC_LONG(param);
            if (readable)                                        /* current */
                propertyValue = cString::FromType(g_value_get_long(&value));

            minimum = pLong->minimum;
            maximum = pLong->maximum;
            propertyDefaultValue = cString::FromType((tUInt32)pLong->default_value);

            propertyType = "tInt64";
            break;
        }

        case G_TYPE_UINT:  //  Unsigned Integer
        {
            GParamSpecUInt *pUInt = G_PARAM_SPEC_UINT(param);
            if (readable)                                        /* current */
                propertyValue = cString::FromType(g_value_get_uint(&value));

            minimum = pUInt->minimum;
            maximum = pUInt->maximum;
            propertyDefaultValue = cString::FromType((tUInt32)pUInt->default_value);

            propertyType = "tUInt32";
            break;
        }

        case G_TYPE_INT:  //  Integer
        {
            GParamSpecInt *pInt = G_PARAM_SPEC_INT(param);
            if (readable)                                        /* current */
                propertyValue = cString::FromType(g_value_get_int(&value));

            minimum = pInt->minimum;
            maximum = pInt->maximum;
            propertyDefaultValue = cString::FromType((tInt32)pInt->default_value);
            propertyType = "tInt32";
            break;
        }

        case G_TYPE_UINT64: //  Unsigned Integer64.
        {
            GParamSpecUInt64 *pUInt64 = G_PARAM_SPEC_UINT64(param);
            if (readable)                                        /* current */
                propertyValue = cString::FromType((tUInt64)g_value_get_uint64(&value));

            minimum = static_cast<tFloat64>(pUInt64->minimum);
            maximum = static_cast<tFloat64>(pUInt64->maximum);
            propertyDefaultValue = cString::FromType((tUInt64)pUInt64->default_value);

            propertyType = "tUInt64";
            break;
        }

        case G_TYPE_INT64: // Integer64
        {
            GParamSpecInt64 *pInt64 = G_PARAM_SPEC_INT64(param);
            if (readable)                                        /* current */
                propertyValue = cString::FromType(g_value_get_int64(&value));

            minimum = static_cast<tFloat64>(pInt64->minimum);
            maximum = static_cast<tFloat64>(pInt64->maximum);
            propertyDefaultValue = cString::FromType((tInt64)pInt64->default_value);

            propertyType = "tInt64";
            break;
        }

        case G_TYPE_FLOAT:  //  Float.
        {
            GParamSpecFloat *pFloat = G_PARAM_SPEC_FLOAT(param);
            if (readable)                                        /* current */
                propertyValue = cString::FromType(g_value_get_float(&value));

            minimum = pFloat->minimum;
            maximum = pFloat->maximum;
            propertyDefaultValue = cString::FromType((tFloat32)pFloat->default_value);

            propertyType = "tFloat32";
            break;
        }

        case G_TYPE_DOUBLE:  //  Double
        {
            GParamSpecDouble *pDouble = G_PARAM_SPEC_DOUBLE(param);
            if (readable)                                        /* current */
                propertyValue = cString::FromType(g_value_get_double(&value));

            minimum = pDouble->minimum;
            maximum = pDouble->maximum;
            propertyDefaultValue = cString::FromType((tFloat64)pDouble->default_value);

            propertyType = "tFloat64";
            break;
        }

        default:
            if (param->value_type == GST_TYPE_CAPS)
            {
                const GstCaps *caps = gst_value_get_caps(&value);
                //if (!caps)


            }
            break;
        }

        cGStreamerProperty oProperty;
        oProperty.name = name;
        oProperty.value = propertyValue;
        oProperty.defaultValue = propertyDefaultValue;
        oProperty.maximum = maximum;
        oProperty.minimum = minimum;
        oProperty.type = propertyType;
        oProperty.writable = writable;
        m_mapProperties[name] = oProperty;

        g_value_reset(&value);
    }
    
    g_free(propertySpecs);
}

void cGStreamerReflection::ParseElementPads(GstElement * pElement)
{
    GstIterator* it = gst_element_iterate_pads(pElement);
    if(it)
    {
        GValue p;
        p.g_type = 0;
        while (gst_iterator_next(it, &p) == GST_ITERATOR_OK)
        {
            cPads oPads;
            auto pPad = static_cast<GstPad*>(g_value_get_object(&p));
            GstCaps* pCaps = gst_pad_get_pad_template_caps(pPad);

            oPads.m_strName = gchar_to_string(gst_pad_get_name(pPad));
            oPads.m_strCapsDescription = gchar_to_string(gst_caps_to_string(pCaps));

            if(gst_pad_get_direction(pPad) == GST_PAD_SINK)
            {
                m_lstInputs.push_back(oPads);
            }
            else
            {
                m_lstOutputs.push_back(oPads);
            }

            gst_object_unref(pPad);
        }
    }
    gst_iterator_free(it);
}
std::string cGStreamerReflection::GetMetadata(GstElement * pElement, const char* strMetadataName)
{
    return gchar_to_string(gst_element_get_metadata(pElement, strMetadataName));
}

std::map<std::string, std::string> cGStreamerReflection::ListElements()
{
    int plugincount = 0, featurecount = 0, blacklistcount = 0;
    GList *plugins;
    gchar **types = NULL;

    std::map<std::string, std::string> oPlugins;

    auto pRegistry = gst_registry_get();
    plugins = gst_registry_get_plugin_list(pRegistry);
    while (plugins) 
    {
        GList *features, *orig_features;
        GstPlugin *plugin;

        plugin = (GstPlugin *)(plugins->data);
        plugins = g_list_next(plugins);
        plugincount++;

        if (GST_OBJECT_FLAG_IS_SET(plugin, GST_PLUGIN_FLAG_BLACKLISTED)) {
            blacklistcount++;
            continue;
        }

        orig_features = features =
            gst_registry_get_feature_list_by_plugin(gst_registry_get(),
                gst_plugin_get_name(plugin));
        
        while (features) 
        {
            if (G_UNLIKELY(features->data == NULL))
                continue;
            GstPluginFeature *feature = GST_PLUGIN_FEATURE(features->data);
            featurecount++;

            std::string strPluginName;
            std::string strObjectName;
            std::string strMetaName;

            if (GST_IS_ELEMENT_FACTORY(feature)) {
                GstElementFactory *factory;

                factory = GST_ELEMENT_FACTORY(feature);
                
                strPluginName = gst_plugin_get_name(plugin);
                strObjectName = GST_OBJECT_NAME(feature);
                strMetaName = gst_element_factory_get_metadata(factory,
                    GST_ELEMENT_METADATA_LONGNAME);
            }
            /*else if (GST_IS_TYPE_FIND_FACTORY(feature)) {
                GstTypeFindFactory *factory;
                const gchar *const *extensions;

                if (types)
                    continue;
                factory = GST_TYPE_FIND_FACTORY(feature);
                
                strPluginName = gst_plugin_get_name(plugin);
                strObjectName = gst_plugin_feature_get_name(feature);

                extensions = gst_type_find_factory_get_extensions(factory);
                if (extensions != NULL) 
                {
                    
                }
                else 
                {
                    
                }
            }
            else {
                if (types)
                    continue;

                strPluginName = gst_plugin_get_name(plugin);
                strObjectName = GST_OBJECT_NAME(feature);
            }*/

            oPlugins[strObjectName] = strMetaName;

            features = g_list_next(features);
        }

        gst_plugin_feature_list_free(orig_features);
    }

    gst_plugin_list_free(plugins);
    g_strfreev(types);

    return oPlugins;
}