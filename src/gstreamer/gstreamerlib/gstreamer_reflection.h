/**
 * Copyright 2019 Sebastian Geißler <mail@sebastiangeissler.de>
 *
 * (https://opensource.org/licenses/MIT)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <gst/gst.h>
#include <adtf_filtersdk.h>

using namespace adtf::util;

class cGStreamerReflection
{
public:
    class cGStreamerProperty
    {
    public:
        std::string name;
        std::string value;
        std::string defaultValue;
        std::string type;
        tFloat64 minimum;
        tFloat64 maximum;
        bool writable;
    };

    class cPads
    {
    public:
        std::string m_strName;
        std::string m_strValue;
        std::string m_strCapsDescription;
    };

    std::map<std::string, cGStreamerProperty> m_mapProperties;
    std::vector< cPads > m_lstInputs;
    std::vector< cPads > m_lstOutputs;

    std::string m_strAuthor;
    std::string m_strDescription;
    std::string m_strDocu;
    std::string m_strIconName;
    std::string m_strKlass;
    std::string m_strLongName;

public:
    std::map<std::string, cGStreamerProperty>& GetProperties();

    std::string GetMetadata(GstElement * pElement, const char* strMetadataName);

    void ParseElement(GstElement * element);
    void ParseElement(std::string strElement);
    void ParseElementPads(GstElement * element);
    void ParseElementProperties(GstElement * element);

    std::map<std::string, std::string> ListElements();

};