import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.3
import QtQuick.XmlListModel 2.12

import Controls 1.0
import Utilities 1.0
import EditorPlugin 1.0
import Dialog 1.0
import Helpers 1.0
import Validation 1.0
import ModelTree 1.0
import Utils 1.0

import GStreamerInfo 1.0

EditorPluginBase {
    id: root

    Dialog
    {
        id: dialog
        title: qsTr("Create pins from mapping file...")
        objectName: "create_pins_from_map_file"
        standardButtons: StandardButton.Ok | StandardButton.Cancel

        width: 1200
        height: 900

        property string gstInspect: 'C:/gstreamer/1.0/x86_64/bin/gst-inspect-1.0.exe'
        property string selectedElement;
        property var selectedElementData;

        Component.onCompleted:
        {
            selectedElement = root.getPropertyValue(root.targetModel, "element_factory")
            modelmanager.clearComponentCache()
            update()
        }

        onSelectedElementChanged:
        {
            selectedElementData = gStreamerInfo.parseElement(selectedElement)
            propertiesModel.clear()
            inputPadModel.clear()
            outputPadModel.clear()

            if(selectedElementData && selectedElementData.size())
            {
                console.warn(propertiesModel.count)
                for (const [key, value] of Object.entries(selectedElementData["properties"]))
                {
                    if(key &&
                       key != "name" &&
                       key != "parent" &&
                       value.writable)
                    {
                        propertiesModel.append(value)
                    }
                }
                for (const [key, value] of Object.entries(selectedElementData["input_pads"]))
                {
                    inputPadModel.append(value)
                }
                for (const [key, value] of Object.entries(selectedElementData["output_pads"]))
                {
                    outputPadModel.append(value)
                }
            }

        }

        function update()
        {
            libraryModel.clear()
            for (const [key, value] of Object.entries(gStreamerInfo.getElements()))
            {
                if(key)
                {
                    libraryModel.append({name: key, description: value})
                }
            }

        }

        GStreamerInfo
        {
            id: gStreamerInfo
        }

        ListModel
        {
            id: libraryModel
            ListElement {
                name: "A Masterpiece"
                description: "Gabriel"
            }
        }

        ListModel
        {
            id: propertiesModel
            ListElement {
                name: "name"
                value: "value"
                maximum: 1.0
                minimum: 0.0
                defaultValue: "defaultValue"
                type: "type"
            }
        }

        ListModel
        {
            id: inputPadModel
            ListElement
            {
                name: "x"
                caps_description: "y"
            }
        }

        ListModel
        {
            id: outputPadModel
            ListElement
            {
                name: "xx"
                caps_description: "yy"
            }
        }

        RowLayout
        {
            anchors.fill: parent

            FilteredTreeView
            {
                id: treeview

                Layout.fillHeight: true
                Layout.minimumWidth: 350

                model: libraryModel

                Component.onCompleted:
                {
                    treeview.table.selection.currentChanged.connect(function(index)
                    {
                        let mindex = index.model.mapToSource(index)
                        let item = libraryModel.get(mindex.row)
                        dialog.selectedElement = item.name
                    })
                }

                TableViewColumn {
                    role: "name"
                    title: "Name"
                    width: 175
                }
                TableViewColumn {
                    role: "description"
                    title: "Description"
                    width: 175
                }
            }
            ScrollView
            {
                id: scrollView
                Layout.fillHeight: true
                Layout.fillWidth: true
                ColumnLayout
                {
                    width: scrollView.width - 20

                    Text
                    {
                        text: "Name:"
                        font.bold: true
                    }
                    Text
                    {
                        text: dialog.selectedElement + (dialog.selectedElementData["long_name"] ? "(" + dialog.selectedElementData["long_name"] +")" : "")
                    }
                    Text
                    {
                        text: "Description:"
                        font.bold: true
                    }
                    Text
                    {
                        text: dialog.selectedElementData["description"]
                    }

                    Text
                    {
                        text: "Doku:"
                        font.bold: true
                    }
                    Text
                    {
                        text: dialog.selectedElementData["docu"]
                    }

                    Text
                    {
                        text: "Klass:"
                        font.bold: true
                    }
                    Text
                    {
                        text: dialog.selectedElementData["klass"]
                    }
                    Text
                    {
                        text: "Icon:"
                        font.bold: true
                    }
                    Text
                    {
                        text: dialog.selectedElementData["icon"]
                    }

                    Component
                    {
                        id: pad
                        ColumnLayout
                        {
                            Layout.fillWidth: true
                            Text
                            {
                                text: name
                                font.bold: true
                            }
                            Text
                            {
                                Layout.fillWidth: true

                                height: 75

                                text: caps_description.replace(";", ";\n\n")
                                wrapMode: Text.WordWrap
                            }
                        }
                    }

                    Text
                    {
                        text: "Input Pads:"
                        font.bold: true
                    }
                    RowLayout
                    {
                        Layout.fillWidth: true
                        Item
                        {
                            width: 30
                            height: 30
                        }
                        ColumnLayout
                        {
                            Layout.fillWidth: true
                            Repeater
                            {
                                model: inputPadModel
                                delegate: pad
                            }
                        }

                    }
                    Text
                    {
                        text: "Output Pads:"
                        font.bold: true
                    }
                    RowLayout
                    {
                        Item
                        {
                            width: 30
                            height: 30
                        }
                        ColumnLayout
                        {
                            Layout.fillWidth: true
                            Repeater
                            {
                                model: outputPadModel
                                delegate: pad
                            }
                        }
                    }
                    Text
                    {
                        text: "Properties:"
                        font.bold: true
                    }

                    FilteredTreeView
                    {
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.minimumWidth: 300
                        Layout.minimumHeight: 500

                        model: propertiesModel

                        TableViewColumn {
                            role: "name"
                            title: "Name"
                            width: 150
                        }
                        TableViewColumn {
                            role: "value"
                            title: "Value"
                            width: 75
                        }
                        TableViewColumn {
                            role: "type"
                            title: "Type"
                            width: 75
                        }
                        TableViewColumn {
                            role: "maximum"
                            title: "Maximum"
                            width: 75
                        }
                        TableViewColumn {
                            role: "minimum"
                            title: "Minimum"
                            width: 75
                        }
                        TableViewColumn {
                            role: "defaultValue"
                            title: "DefaultValue"
                            width: 75
                        }
                    }
                }
            }
        }
        onAccepted:
        {
            for(let i = 0; i < propertiesModel.count; i++)
            {
                let prop = propertiesModel.get(i)
                if(prop.type)
                {
                    root.createProperty(root.targetModel, "gst/"+prop.name, prop.defaultValue, prop.type, "")
                }
            }

            root.setProperty(root.targetModel, "element_factory", dialog.selectedElement)
        }
    }

    onExecute: {
        dialog.open()
    }
}
