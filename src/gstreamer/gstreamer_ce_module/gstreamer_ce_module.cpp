/**
 * Copyright 2019 Sebastian Gei�ler <mail@sebastiangeissler.de>
 *
 * (https://opensource.org/licenses/MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <QtQml>
#include "gstreamer_ce_module.h"
#include <gstreamer_reflection.h>


void cGstreamerCeModule::registerTypes(const char *uri)
{
    Q_ASSERT(uri == QLatin1String("GStreamerCeModule"));

    qmlRegisterType(QUrl("qrc:/GStreamerCeModule/GStreamerCeModule.qml"), uri, 1, 0, "GStreamerCeModule");
    qmlRegisterType<cGStreamerInfo>("GStreamerInfo", 1, 0, "GStreamerInfo");
}


cGStreamerInfo::cGStreamerInfo()
{
    gst_init(0, nullptr);
}

cGStreamerInfo::~cGStreamerInfo()
{
    
}

QVariantMap cGStreamerInfo::getElements()
{
    QVariantMap oMap;
    for(auto oElement : m_oGStreamerReflection.ListElements())
    {
        oMap.insert(oElement.first.c_str(), oElement.second.c_str());
    }

    return oMap;
}

QVariantMap cGStreamerInfo::parseElement(QString strName)
{
    QVariantMap oResult;
    try
    {
        m_oGStreamerReflection.ParseElement(strName.toStdString());

        

        QVariantMap oProperties;
        for (auto oEntry : m_oGStreamerReflection.GetProperties())
        {
            QVariantMap oProperty;
            oProperty["name"] = QString::fromStdString(oEntry.second.name);
            oProperty["value"] = QString::fromStdString(oEntry.second.value);
            oProperty["maximum"] = oEntry.second.maximum;
            oProperty["minimum"] = oEntry.second.minimum;
            oProperty["defaultValue"] = QString::fromStdString(oEntry.second.defaultValue);
            oProperty["type"] = QString::fromStdString(oEntry.second.type);
            oProperty["writable"] = oEntry.second.writable;
            oProperties[QString::fromStdString(oEntry.first)] = oProperty;
        }
        oResult["properties"] = oProperties;

        QVariantMap oInputPads;
        for (auto oPad : m_oGStreamerReflection.m_lstInputs)
        {
            QVariantMap oPadVariant;
            oPadVariant["name"] = QString::fromStdString(oPad.m_strName);
            oPadVariant["caps_description"] = QString::fromStdString(oPad.m_strCapsDescription);
            oInputPads[QString::fromStdString(oPad.m_strName)] = oPadVariant;
        }
        oResult["input_pads"] = oInputPads;

        QVariantMap oOutputPads;
        for (auto oPad : m_oGStreamerReflection.m_lstOutputs)
        {
            QVariantMap oPadVariant;
            oPadVariant["name"] = QString::fromStdString(oPad.m_strName);
            oPadVariant["caps_description"] = QString::fromStdString(oPad.m_strCapsDescription);
            oOutputPads[QString::fromStdString(oPad.m_strName)] = oPadVariant;
        }
        oResult["output_pads"] = oOutputPads;

        oResult["author"] = QString::fromStdString(m_oGStreamerReflection.m_strAuthor);
        oResult["description"] = QString::fromStdString(m_oGStreamerReflection.m_strDescription);
        oResult["docu"] = QString::fromStdString(m_oGStreamerReflection.m_strDocu);
        oResult["icon"] = QString::fromStdString(m_oGStreamerReflection.m_strIconName);
        oResult["klass"] = QString::fromStdString(m_oGStreamerReflection.m_strKlass);
        oResult["longName"] = QString::fromStdString(m_oGStreamerReflection.m_strLongName);

        
    }
    catch(tResult oResult)
    {
        LOG_RESULT(oResult);
    }
    return oResult;
}