# ADTF3 Video Toolbox

## A GStreamer filter

The GStreamer filter offers the integration of the gstreamer pipeline in ADTF. 
This allows the creation of a GStreamer pipeline within an ADTF filter graph, 
whereby the Gstreamer filter represents a single gstreamer element. 
GStreamer Sink and Source Filter allows you to convert ADTF samples into Gstreamer samples and vice versa.
All gstreamer elements can be parameterized in ADTF via properties.

GStreamer is a library with a large number of plugins for processing video and audio streams.
This includes compressing in h264 h265 ... as well as playing and saving in common container formats such as .mp4 .avi ... 
or accessing video streams from network via RSTP ...

For more details see: 
https://gstreamer.freedesktop.org

## How to install

* Download from https://gstreamer.freedesktop.org/download/ 
** For Windows choose "MSVC 64-bit (VS 2019, Release CRT)"
* Install GStreamer and add the bin folder of the installation into your system PATH variable:
** PATH += C:/gstreamer/1.0/x86_64/bin

## How to use

![Example Video](doc/images/example.gif)

## Whats comming next: A set of OpenCV filter

Gives ADTF3 access to OpenCV functions like:

* Deep Neuronal Network 
* Resize
* Camera Source
* Image Source
* Image to Mat
* Mat to Image
* Hough line / circel detection (not jet)
* Canny Filter (not jet)

and many more
