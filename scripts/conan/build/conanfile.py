from conans import ConanFile, tools, CMake
import os

class ADTF3VideoToolbox(ConanFile):
    name = "adtf_video_toolbox"
    version = "0.1.0"
    
    settings = "os", "compiler", "build_type", "arch"
    description = "%s" % (name)
    generators = "cmake", "txt", "virtualenv"
    short_paths = True
    keep_imports = True
    no_copy_source = True
    enable_multiconfig_package = False
    
    scm = {
        "type": "git",
        "subfolder": ".",
        "url": "auto",
        "revision": "auto"
    }
    
    build_requires = "ADTF/3.12.0@dw/stable", "opencv/4.5.0@sebastian/testing", "Qt/5.12.9@dw/stable"
    
    def build(self):
        cmake = CMake(self)
        cmake.definitions["CUDA_TOOLKIT_ROOT_DIR"] = "C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v10.2"
        cmake.definitions["GSTREAMER_DIR:PATH"] = "C:/gstreamer/1.0/x86_64"
        cmake.definitions["QT_DIR:PATH"] = self.deps_cpp_info["Qt"].rootpath
        cmake.definitions["CMAKE_PREFIX_PATH:PATH"] = self.deps_cpp_info["Qt"].rootpath
        cmake.configure()
        cmake.build()
        
        cmake.install()
    

