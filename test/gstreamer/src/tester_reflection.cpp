/**
 *
 * @file
 * Copyright &copy; Audi Electronics Venture GmbH. All rights reserved
 *
 */


#include <adtf_filtersdk.h>
#include <adtfsystemsdk/testing/test_system.h>
#include <adtftesting/adtf_testing.h>
#include <gstreamer_reflection.h>

using namespace adtf::util;
using namespace adtf::ucom;
using namespace adtf::base;
using namespace adtf::testing;
using namespace adtf::streaming::testing;
using namespace adtf::streaming;
using namespace adtf::filter::testing;

TEST_CASE("list all elements")
{
    cSystem::SetEnvVariable("GST_DEBUG", "3");
    gst_init(0,nullptr);
    cGStreamerReflection oReflection;
    auto lstElements = oReflection.ListElements();

    REQUIRE(lstElements.size() > 0);

    REQUIRE("Fake Sink" == lstElements["fakesink"]);
  
}

TEST_CASE("get element info")
{
    gst_init(0, nullptr);
    cGStreamerReflection oReflection;
    oReflection.ParseElement("fakesink");
    REQUIRE("TempObject" == oReflection.GetProperties()["name"].value);

    REQUIRE(oReflection.m_strDescription == "Black hole for data");

    REQUIRE(oReflection.m_lstInputs.size() == 1);
    REQUIRE(oReflection.m_lstInputs[0].m_strName == "sink");
    REQUIRE(oReflection.m_lstInputs[0].m_strCapsDescription == "ANY");

    oReflection.ParseElement("fakesrc");
    REQUIRE(oReflection.m_lstOutputs.size() == 1);
    REQUIRE(oReflection.m_lstOutputs[0].m_strName == "src");
    REQUIRE(oReflection.m_lstOutputs[0].m_strCapsDescription == "ANY");

}
