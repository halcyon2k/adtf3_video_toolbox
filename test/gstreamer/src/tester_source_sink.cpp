/**
 *
 * @file
 * Copyright &copy; Audi Electronics Venture GmbH. All rights reserved
 *
 */


#include <adtf_filtersdk.h>
#include <adtfsystemsdk/testing/test_system.h>
#include <adtftesting/adtf_testing.h>

using namespace adtf::util;
using namespace adtf::ucom;
using namespace adtf::base;
using namespace adtf::testing;
using namespace adtf::streaming::testing;
using namespace adtf::streaming;
using namespace adtf::filter::testing;

/**
 * Send adtf/image through gstreamer-appsource and gstreamer_appsink 
 */
TEST_CASE("appsink_appsource")
{
    cSystem::SetEnvVariable("ADTF_VIDEO_TOOLBOX_DIR", ADTF_VIDEO_TOOLBOX_DIR);

    adtf::system::testing::cSessionTestSystem oSession;
    oSession.CreateSession(std::string(ADTF_TESTING_SOURCE_DIR) + "/../files/gstreamer/adtfsessions/sink_source.adtfsession");
    oSession.ExecTest(tADTFRunLevel::RL_FilterGraph, [&]()
    {
        object_ptr<IFilter> pSource;
        oSession.GetFilterGraph()->GetNamedGraphObject("gstreamer_source", pSource);
        cOutputRecorder oOutput(pSource, "outpin");
        oSession.SetRunLevel(tADTFRunLevel::RL_Running);

        oOutput.WaitForTrigger(std::chrono::seconds(20));

        auto oCurrentOutput = oOutput.GetCurrentOutput();

        std::string strMetaTypeName;
        REQUIRE(oCurrentOutput.GetTypes().size() > 0);
        auto pStreamType = oCurrentOutput.GetTypes()[0];
        REQUIRE_OK(pStreamType->GetMetaTypeName(adtf_string_intf(strMetaTypeName)));
        REQUIRE(strMetaTypeName == "adtf/image");

        tStreamImageFormat sFormat;
        get_stream_type_image_format(sFormat, *pStreamType);

        REQUIRE(sFormat.m_ui32Width == 640);
        REQUIRE(sFormat.m_ui32Height == 480);
        REQUIRE(sFormat.m_strFormatName == "GREY(8)");

        auto pSamples = oCurrentOutput.GetSamples();
        REQUIRE(pSamples.size() >= 1);

        object_ptr_shared_locked<const ISampleBuffer> pBuffer;
        REQUIRE_OK(pSamples[0]->Lock(pBuffer));

        REQUIRE(pBuffer->GetSize() == 307200);

    });
}

