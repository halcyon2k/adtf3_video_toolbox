import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.0

import QtQuick 2.0
import QtCharts 2.0

Item
{
    id: root
    anchors.fill: parent
    property var names: ["person","bicycle","car","motorbike","aeroplane","bus","train","truck","boat","traffic light","fire hydrant","stop sign","parking meter","bench","bird","cat","dog","horse","sheep","cow","elephant","bear","zebra","giraffe","backpack","umbrella","handbag","tie","suitcase","frisbee","skis","snowboard","sports ball","kite","baseball bat","baseball glove","skateboard","surfboard","tennis racket","bottle","wine glass","cup","fork","knife","spoon","bowl","banana","apple","sandwich","orange","broccoli","carrot","hot dog","pizza","donut","cake","chair","sofa","pottedplant","bed","diningtable","toilet","tvmonitor","laptop","mouse","remote","keyboard","cell phone","microwave","oven","toaster","sink","refrigerator","book","clock","vase","scissors","teddy bear","hair drier","toothbrush"]
    property var classifcationResult: [];

    Component.onCompleted: 
    {
        var video = filter.createInputPin("classes")
        video.sample.connect(function(sample)
        {
            let data = new Float32Array(sample.data)

            var classifcationResult = []
            for(var i = 0; i < data.length; i += 2)
            {
                classifcationResult.push({name:names[data[i]], confidence: data[i+1]})
            }

            classifcationResult = classifcationResult.reduce((total, next) => {
                var foundIndex = -1;
                total.forEach((element, index) => {
                    if(element &&
                       next &&
                       element.name === next.name)
                    {
                        foundIndex = index;
                    }
                })

                if (foundIndex === -1)
                {
                    if(next.name)
                    {
                        total.push(next)
                    }
                }
                else
                {
                    total[foundIndex].confidence = total[foundIndex].confidence + next.confidence
                }
                return total;
            }, []);

            classifcationResult.sort(function(a,b){ return a.name > b.name})

            console.log(JSON.stringify(classifcationResult))
            root.classifcationResult = classifcationResult;
        })
    }

    ChartView
    {
        id: chart

        title: "Production costs"
        anchors.fill: parent
        legend.visible: true
        antialiasing: true

        PieSeries
        {
            id: pieOuter
            size: 0.96
            holeSize: 0.2
            PieSlice { id: slice1; label: (classifcationResult.length <= 0 ? "" : ""+classifcationResult[0].name); value: (classifcationResult.length <= 0 ? 0 : classifcationResult[0].confidence); labelVisible: true; labelPosition: PieSlice.LabelInsideNormal; color: "#99CA53" }
            PieSlice { id: slice2; label: (classifcationResult.length <= 1 ? "" : ""+classifcationResult[1].name); value: (classifcationResult.length <= 1 ? 0 : classifcationResult[1].confidence); labelVisible: true; labelPosition: PieSlice.LabelInsideNormal; color: "#209FDF" }
            PieSlice { id: slice3; label: (classifcationResult.length <= 2 ? "" : ""+classifcationResult[2].name); value: (classifcationResult.length <= 2 ? 0 : classifcationResult[2].confidence); labelVisible: true; labelPosition: PieSlice.LabelInsideNormal; color: "#F6A625" }
            PieSlice { id: slice4; label: (classifcationResult.length <= 3 ? "" : ""+classifcationResult[3].name); value: (classifcationResult.length <= 3 ? 0 : classifcationResult[3].confidence); labelVisible: true; labelPosition: PieSlice.LabelInsideNormal; color: "#63BCE9" }
            PieSlice { id: slice5; label: (classifcationResult.length <= 4 ? "" : ""+classifcationResult[4].name); value: (classifcationResult.length <= 4 ? 0 : classifcationResult[4].confidence); labelVisible: true; labelPosition: PieSlice.LabelInsideNormal; color: "#E9F5FC" }
        }
    }
}
